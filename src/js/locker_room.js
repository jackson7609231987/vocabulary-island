$(function () {
    let member_wardrobe = [];
    let member_character = [];
    let member_blance = 0;
    let user_character = 0;
    /* 跳出通知。*/
    function dialog(status) {
        let icon = "";
        let title = "";
        let text = "";
        let console_log = "";
        if (status == 0) {
            icon = "warning";
            title = "將前往";
            text = "是否前往【登入畫面】？";
            swal.fire({
                    icon: icon,
                    title: title,
                    text: text,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes !'
                })
                .then((result) => {
                    if (result.value) {
                        window.location.assign("index.html");
                    }
                });

        } else if (status == 1) {
            icon = "warning";
            title = "哎呀！發現你沒登入哦！";
            text = "將前往【登入畫面】";
            swal.fire({
                    icon: icon,
                    title: title,
                    text: text,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCloseButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'O K'
                })
                .then((result) => {
                    if (result.value) {
                        window.location.assign("index.html");
                    }
                });
        }






    }

    /*抓使用者資訊。*/
    function getPersonalInformation() {
        console.log("開始抓使用者資訊...");

        $.ajax({
            type: "get",
            async: false, //async設定true會變成異步請求。
            cache: true,
            url: "php/world.php",
            dataType: "json",
            success: function (json) {
                //jQuery會自動將結果傳入(如果有設定callback函式的話，兩者都會執行)
                console.log('Success.');
                name = json['name'];
                number_of_card = json['cardAmount'];
                coin = json['coin'];
                member_blance = parseInt(coin)
                member_wardrobe = json['member_wardrobe'];
                user_wardrobe_id = json['user_wardrobe_id']
                wardrobe_name = json['wardrobe_name']
                user_character = json ['user_character'];
                console.log('Name:' + name);

                if (name == "" || name == null) { // 帳號未登入。
                    dialog(1);
                }
                // 顯示金錢
                $('#coin').text(coin);
                // 顯示帽子
                if (user_wardrobe_id != null) {
                     $('.commodity').css("border", " 8px solid rgb(255, 255, 255)");
                     $(`#${wardrobe_name}`).css("border"," 8px solid rgb(0, 255, 255)");
                     $(`#avatar`).attr("src","./material/locker-room/hat/"+user_character+"_"+wardrobe_name+".png");
                }else{
                    if (user_character==1) {
                        $(`#avatar`).attr("src","./material/locker-room/hat/恐龍.png");
                    }
                    $(`#close`).css("border"," 8px solid rgb(0, 255, 255)");
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
                console.log("textStatus:" + textStatus);
                console.log("errorThrown:" + errorThrown);
                console.log('Error.');

                console.log('Name:' + name);

               // 帳號未登入。
                    dialog(1);

                number_of_card = 0;
                $('#name').text(name);
                $('#flashcard').text(number_of_card);
            }
        });
        /* 未完成 */
    }
    function getAllIteam(){
        console.log("開始抓商品資訊...");

        $.ajax({
            type: "post",
            async: false, //async設定true會變成異步請求。
            cache: true,
            url: "php/shop.php",
            dataType: "json",
            data:{
                code:'GET_ALL'
            },
            success: function (json) {
                let context = "";
                console.log(json.length + " 筆資料 : member_wardrobe : " + member_wardrobe.length);
                let appnenum = 1;
                for (let index = 2; index <= Math.ceil(json.length / 9); index++) {
                    $(`#hat_shop`).append(`<div class="carousel-item" id="hat_shop_item_${index}">
                    <div class="row" id="hat_shop_item_${index}_row_1"><div class="col-1"></div></div></div>`);
                }
                let page=1;
                for (let i = 0; i < json.length; i++) {
                    let have = false;
                    let name = json[i]['name'];
                    member_wardrobe.forEach(element => {
                        if (element.wardrobe_name === name) {
                            have = true
                        }
                    });
                    appnenum++;
                    let img_url = have ? json[i]['img_url'] : json[i]['img_url'].slice(0, json[i]['img_url'].indexOf('.png'))+"_lock"+json[i]['img_url'].slice( json[i]['img_url'].indexOf('.png'));
                    let coin = json[i]['coin'];
                    let col=3;
                    if (appnenum == 2 || appnenum == 5 || appnenum == 8 || appnenum == 11) {
                        col=4
                    }
                    if (appnenum == 10) {
                        page++;
                    }
                    console.log("page:"+page+" appnenum:"+appnenum);
                    let new_context = " <div class='col-"+col+"' ><div class='row'><div class='col-12'><div id='"+name+"' class='commodity' style='background-color: rgb(255, 232, 211);border: 8px solid rgb(255, 255, 255)'><img class='commodity_img' data-have="+have+" data-coin="+coin+" name='"+name+"'src='"+img_url+"' ></div><div class='col-12'><img class='s_coin_icon' src='material/locker-room/dollar.png'><span class='commodity_name'>"+coin+"</span></div></div></div></div>";
                    $(`#hat_shop_item_${page}_row_1`).append(new_context);
                    // $('#hat_shop_item_2_row_1').eq(0).append(new_context);
                    if (appnenum % 3 == 0) {
                        $(`#hat_shop_item_${page}_row_1`).append("<div class='col-1'></div>");
                        $(`#hat_shop_item_${page}_row_1`).append("<div class='col-1'></div>");
                    }
                    
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
                console.log("textStatus:" + textStatus);
                console.log("errorThrown:" + errorThrown);
                console.log('Error.');

            }
        });
    }

    function addEventLog() {
        console.log("開始抓使用者資訊...");

        $.ajax({
            type: "post",
            async: true, //async設定true會變成異步請求。
            cache: true,
            url: "php/event.php",
            data: {
                code: 2,
            },
            dataType: "json",
            success: function (json) {

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
        /* 未完成 */
    }
    /*---------------------------------------------------------------------*/
    getPersonalInformation();
    getAllIteam();
    addEventLog();

    /* 回去英文單字島。 */
    $('#earth').on('click', function () {
        console.log('將進入 - 英文單字島');
        window.location.assign("world.html");
    });

    // 點商品
    $('.commodity_img').on('click', function () {
        console.log('click');
        let img_src = this.src;
        let item_name = this.name;
        console.log(this.src);
        if (!$(this).data('have') && this.name !== 'close' ) {
            console.log(member_blance);
            if (member_blance < parseInt($(this).data('coin'))) {
                alert('金額不足')
            }else{
                console.log('還沒有傭有')
                swal.fire({
                    icon: "",
                    title: "確定要購買？",
                    html:"<img class='s_coin_icon' style='width: 50px;height: 50px;' src='material/locker-room/dollar.png'><p id='alert_coin'>"+$(this).data('coin')+"</p>",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes !'
                })
                .then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "post",
                            async: false, //async設定true會變成異步請求。
                            cache: true,
                            url: "php/shop.php",
                            dataType: "json",
                            data:{
                                code:'BUY_HAT',
                                wardrobe_name:item_name,
                                wardrobe_img_url:img_src
                            },
                            success: function (json) {
                                if (json.msg==='done') {
                                    location.reload();
                                }else{
                                    alert(json)
                                }
    
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
                                console.log("textStatus:" + textStatus);
                                console.log("errorThrown:" + errorThrown);
                                console.log('Error.');
                                location.reload();
                            }
                        })
                    }
                        // window.location.assign("index.html");
                });
            }
        } else {
            console.log('有了，更換投向');
            swal.fire({
                icon: "",
                title: "確定要更換上去嗎？",
                allowOutsideClick: false,
                allowEscapeKey: false,
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes !'
            })
            .then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "post",
                        async: false, //async設定true會變成異步請求。
                        cache: true,
                        url: "php/shop.php",
                        dataType: "json",
                        data:{
                            code:'CHANHE_HAT',
                            wardrobe_name:item_name,
                            wardrobe_img_url:img_src
                        },
                        success: function (json) {
                            if (json.msg==='done') {
                                location.reload();
                            }else{
                                alert(json)
                            }

                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
                            console.log("textStatus:" + textStatus);
                            console.log("errorThrown:" + errorThrown);
                            console.log('Error.');
                        }
                    })
                }
                    // window.location.assign("index.html");
            });
        }

    });

    $('.character_commodity_img').on('click', function () {
        console.log('click');
        let item_name = this.name;
        let character_code;
        console.log(this.src);
        if (item_name == 'panda') {
            character_code=0
        }
        if (item_name == 'dinosaur') {
            character_code=1
        }
        if (item_name == 'kangaroo') {
            character_code=2
        }
        if (item_name == 'rabbit') {
            character_code=3
        }
        if (item_name == 'sheep') {
            character_code=4
        }
        if (!$(this).data('have') && this.name !== 'close' ) {
            console.log(member_blance);
            if (member_blance < parseInt($(this).data('coin'))) {
                alert('金額不足')
            }else{
                console.log('還沒有')
                swal.fire({
                    icon: "",
                    title: "確定要購買？",
                    html:"<img class='s_coin_icon' style='width: 50px;height: 50px;' src='material/locker-room/dollar.png'><p id='alert_coin'>"+$(this).data('coin')+"</p>",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes !'
                })
                .then((result) => {
                    if (result.value) {
                        $.ajax({
                            type: "post",
                            async: false, //async設定true會變成異步請求。
                            cache: true,
                            url: "php/shop.php",
                            dataType: "json",
                            data:{
                                code:'BUY_CHARACTER',
                                character_code
                            },
                            success: function (json) {
                                if (json.msg==='done') {
                                    location.reload();
                                }else{
                                    alert(json)
                                }
    
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
                                console.log("textStatus:" + textStatus);
                                console.log("errorThrown:" + errorThrown);
                                console.log('Error.');
                                location.reload();
                            }
                        })
                    }
                        // window.location.assign("index.html");
                });
            }
        } else {
            console.log('有了，更換投向');
            swal.fire({
                icon: "",
                title: "確定要更換上去嗎？",
                allowOutsideClick: false,
                allowEscapeKey: false,
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes !'
            })
            .then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "post",
                        async: false, //async設定true會變成異步請求。
                        cache: true,
                        url: "php/shop.php",
                        dataType: "json",
                        data:{
                            code:'CHANHE_CHARACTER',
                            character_code
                        },
                        success: function (json) {
                            if (json.msg==='done') {
                                location.reload();
                            }else{
                                alert(json)
                            }

                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
                            console.log("textStatus:" + textStatus);
                            console.log("errorThrown:" + errorThrown);
                            console.log('Error.');
                        }
                    })
                }
                    // window.location.assign("index.html");
            });
        }
    });

    $('#btnradio1').on('click', function () {
        $('#hat').css('display','block');
        $('#character').css('display','none');
    });


    $('#btnradio2').on('click', function () {
        $('#hat').css('display','none');
        $('#character').css('display','block');
        // 顯示帽子
        if (user_character=="0") {
            $(`#panda`).css("border"," 8px solid rgb(0, 255, 255)");
        }
        if (user_character=="1") {
            $('#dinosaur').css("border"," 8px solid rgb(0, 255, 255)");
        }
        if (user_character=="2") {
            $('#kangaroo').css("border"," 8px solid rgb(0, 255, 255)");
        }
        if (user_character=="3") {
            $('#rabbit').css("border"," 8px solid rgb(0, 255, 255)");
        }
        if (user_character=="4") {
            $('#sheep').css("border"," 8px solid rgb(0, 255, 255)");
        }
    });




});