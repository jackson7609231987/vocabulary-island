$(function () {
    /* 跳出通知。*/
    function dialog(status) {
        let icon = "";
        let title = "";
        let text = "";
        let console_log = "";
        if (status == 0) {
            icon = "warning";
            title = "將前往";
            text = "是否前往【登入畫面】？";
            swal.fire({
                    icon: icon,
                    title: title,
                    text: text,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes !'
                })
                .then((result) => {
                    if (result.value) {
                        window.location.assign("index.html");
                    }
                });

        } else if (status == 1) {
            icon = "warning";
            title = "哎呀！發現你沒登入哦！";
            text = "將前往【登入畫面】";

            swal.fire({
                    icon: icon,
                    title: title,
                    text: text,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCloseButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'O K'
                })
                .then((result) => {
                    if (result.value) {
                        window.location.assign("index.html");
                    }
                });
        }






    }
    /*抓使用者資訊。*/
    function getPersonalInformation() {
        console.log("開始抓使用者資訊...");

        $.ajax({
            type: "get",
            async: false, //async設定true會變成異步請求。
            cache: true,
            url: "php/world.php",
            dataType: "json",
            success: function (json) {
                //jQuery會自動將結果傳入(如果有設定callback函式的話，兩者都會執行)
                console.log('Success.');
                name = json['name'];
                number_of_card = json['cardAmount'];
                coin = json['coin'];

                console.log('Name:' + name);

                if (name == "" || name == null) { // 帳號未登入。
                    dialog(1);
                }

                $('#name').text(name);
                $('#flashcard').text(number_of_card);
                $('#coin').text(coin);

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
                console.log("textStatus:" + textStatus);
                console.log("errorThrown:" + errorThrown);
                console.log('Error.');

                console.log('Name:' + name);

                dialog(1);

                number_of_card = 0;
                $('#name').text(name);
                $('#flashcard').text(number_of_card);
            }
        });
        /* 未完成 */
    }
    /*---------------------------------------------------------------------*/
    function addEventLog() {
        console.log("開始抓使用者資訊...");

        $.ajax({
            type: "post",
            async: true, //async設定true會變成異步請求。
            cache: true,
            url: "php/event.php",
            data: {
                code: 0,
            },
            dataType: "json",
            success: function (json) {

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
        /* 未完成 */
    }

    function getRank() {
        console.log("開始抓使用者資訊...");

        $.ajax({
            type: "get",
            async: true, //async設定true會變成異步請求。
            cache: true,
            url: "php/rank.php",
            dataType: "json",
            success: function (json) {
                let context = "";
                //jQuery會自動將結果傳入(如果有設定callback函式的話，兩者都會執行)
                console.log(json.length + " 筆資料");

                json.sort(function(a, b) {
                    return b.count - a.count;
                });
                console.log(json);
                for (let i = 0; i < json.length; i++) {
                    let name = json[i]['user_name'];
                    let count = json[i]['count'];
                    let user_wardrobe = json[i]['user_wardrobe'] ? json[i]['user_wardrobe'] : 'hat_0'
                    let user_character = json[i]['user_character'];
                    if (i<3) {
                        $(`.top${i+1}`).append(`<img class="top${i+1}-img"src="./material/locker-room/hat/${user_character}_${user_wardrobe}.png" alt="">`);

                    }
                    $(`.name_card`).append(`<dic class='row card-item'><div class="col-1"></div><div class="col-2 title-value"><img class='card_trophy' src='./material/獎盃/獎盃_${(i+1)}.png' ></div><div class="col-3 title-value">${name}</div><div class="col-2"></div><div class="col-3 title-value">${count}</div><div class="col-1"></div></div>`);
                    // let new_context = "<div  class='card'  style='background-color: white; '><div><img class='card_trophy' src='./material/獎盃/獎盃_"+(i+1)+".png' >"
                    // +"  <p class='card_name' >"+name+"</p>"
                    // +"  <p class='card_count' >"+count+"</p></div></div>";
                    // context = context + new_context;
                }
                /* 渲染於瀏覽器上。 */
                // $('#rank_pile').append(context);
                $('body').loading('stop');

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
                console.log("textStatus:" + textStatus);
                console.log("errorThrown:" + errorThrown);
                console.log('Error.');
                console.log('Name:' + name);
                number_of_card = 0;
            }
        });
        /* 未完成 */
    }

    /* 回去英文單字島。 */
    $('#earth').on('click', function () {
        console.log('將進入 - 英文單字島');
        window.location.assign("world.html");
    });
    addEventLog();
    getPersonalInformation();
    getRank();
    
});