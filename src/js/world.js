$(async function () {
    let account = ""; // 輸入的帳號。
    let name = ""; // 使用者姓名。
    let number_of_card = 0; // 字卡數量。
    var islands_status = []; // 每個島嶼進度。
    var now_island = [];
    var all_islands = [];
    var page = 0;


    /* 跳出通知。*/
    function dialog(status) {
        let icon = "";
        let title = "";
        let text = "";
        let console_log = "";
        if (status == 0) {
            icon = "warning";
            title = "將前往";
            text = "是否前往【登入畫面】？";
            swal.fire({
                icon: icon,
                title: title,
                text: text,
                allowOutsideClick: false,
                allowEscapeKey: false,
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes !'
            })
            .then((result) => {
                if (result.value) {
                    $.ajax({
                        type: "get",
                        async: false, //async設定true會變成異步請求。
                        cache: true,
                        url: "php/logout.php",
                        dataType: "json",
                        success: function (json) {}
                    });
                        window.location.assign("index.html");
                    }
                });

        } else if (status == 1) {
            icon = "warning";
            title = "哎呀！發現你沒登入哦！";
            text = "將前往【登入畫面】";

            swal.fire({
                    icon: icon,
                    title: title,
                    text: text,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCloseButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'O K'
                })
                .then((result) => {
                    if (result.value) {
                        window.location.assign("index.html");
                    }
                });
        }






    }

    /*抓使用者資訊。*/
    async function getPersonalInformation() {
        console.log("開始抓使用者資訊...");

        await $.ajax({
            type: "get",
            async: true, //async設定true會變成異步請求。
            cache: true,
            url: "php/world.php",
            dataType: "json",
            success: function (json) {
                //jQuery會自動將結果傳入(如果有設定callback函式的話，兩者都會執行)
                console.log('Success.');
                name = json['name'];
                number_of_card = json['cardAmount'];
                coin = json['coin'];
                all_islands = json['island'];
                wardrobe_name = json['wardrobe_name']
                user_wardrobe_id = json['user_wardrobe_id']
                user_character = json ['user_character'];
                console.log('Name:' + name);

                if (name == "" || name == null) { // 帳號未登入。
                    dialog(1);
                }

                $('#name').text(name);
                $('#flashcard').text(number_of_card);
                $('#coin').text(coin);
                 // 顯示帽子
                if (user_wardrobe_id != null) {
                    $('.commodity').css("border", " 8px solid rgb(255, 255, 255)");
                    $(`#${wardrobe_name}`).css("border"," 8px solid rgb(0, 255, 255)");
                    $(`#avatar`).attr("src","./material/locker-room/hat/"+user_character+"_"+wardrobe_name+".png");
               }else{
                   if (user_character==1) {
                       $(`#avatar`).attr("src","./material/locker-room/hat/恐龍.png");
                   }
                   $(`#close`).css("border"," 8px solid rgb(0, 255, 255)");
               }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
                console.log("textStatus:" + textStatus);
                console.log("errorThrown:" + errorThrown);
                console.log('Error.');

                console.log('Name:' + name);

                if (name == "" || name == null) { // 帳號未登入。
                    dialog(1);
                }

                number_of_card = 0;
                $('#name').text(name);
                $('#flashcard').text(number_of_card);
            }
        });
        /* 未完成 */
    }

    // 產生島嶼
    async function show_this_page_island(page_number) {
        if (!page_number) {
            page_number = all_islands.length
        }
        console.log(page_number);
        let island_number = 0;
        // 計算總頁面數 / 5
        for (let index = 0; index < Math.ceil(page_number / 5); index++) {
            console.log("slide_"+(index+1));
            $("#slides").append("<div id='slide_"+(index+1)+"' class='slide fp-auto-height'></div>");
            // 每個頁面放五座島
            let buliding = "";
            for (let iland_index = 0; iland_index < 5; iland_index++) {
                // 如果超過總島數量就跳出迴圈
                if (island_number > page_number -1 ) {
                    break;
                }
                let isiland_name = all_islands[island_number].title_name ;
                let isiland_recod_percentage = (all_islands[island_number].record_totle_count / all_islands[island_number].totle_count)*100;
                console.log('isiland_name : '+isiland_name+" isiland_recod_percentage:"+isiland_recod_percentage );
                if (isiland_recod_percentage == 100) {
                    if (isiland_name === 'Animals and Insects') {
                        buliding = "material/island/animals/bu_island_7.png"
                    }else{
                        buliding = "material/island/bu_island_7.png"
                    }
                    if (isiland_name === 'Food') {
                        buliding = "material/island/food/bu_island_7.png"
                    }
                    if (isiland_name === 'School') {
                        buliding = "material/island/school/bu_island_7.png"
                    }
                } else if (isiland_recod_percentage >= 85 && isiland_recod_percentage < 100) {
                    if (isiland_name === 'Animals and Insects') {
                        buliding = "material/island/animals/bu_island_6.png"
                    }else{
                        buliding = "material/island/bu_island_6.png"
                    }
                    if (isiland_name === 'Food') {
                        buliding = "material/island/food/bu_island_7.png"
                    }
                    if (isiland_name === 'School') {
                        buliding = "material/island/school/bu_island_7.png"
                    }

                } else if (isiland_recod_percentage >= 68 && isiland_recod_percentage < 85) {
                    if (isiland_name === 'Animals and Insects') {
                        buliding = "material/island/animals/bu_island_6.png"
                    }else{
                        buliding = "material/island/bu_island_6.png"
                    }
                    if (isiland_name === 'Food') {
                        buliding = "material/island/food/bu_island_6.png"
                    }
                    if (isiland_name === 'School') {
                        buliding = "material/island/school/bu_island_6.png"
                    }
                } else if (isiland_recod_percentage >= 51 && isiland_recod_percentage < 68) {
                    if (isiland_name === 'Animals and Insects') {
                        buliding = "material/island/animals/bu_island_5.png"
                    }else{
                        buliding = "material/island/bu_island_5.png"
                    }
                    if (isiland_name === 'Food') {
                        buliding = "material/island/food/bu_island_5.png"
                    }
                    if (isiland_name === 'School') {
                        buliding = "material/island/school/bu_island_5.png"
                    }
                } else if (isiland_recod_percentage >= 34 && isiland_recod_percentage < 51) {
                    if (isiland_name === 'Animals and Insects') {
                        buliding = "material/island/animals/bu_island_4.png"
                    }else{
                        buliding = "material/island/bu_island_4.png"
                    }
                    if (isiland_name === 'Food') {
                        buliding = "material/island/food/bu_island_4.png"
                    }
                    if (isiland_name === 'School') {
                        buliding = "material/island/school/bu_island_4.png"
                    }
                } else if (isiland_recod_percentage >= 17 && isiland_recod_percentage < 34) {
                    if (isiland_name === 'Animals and Insects') {
                        buliding = "material/island/animals/bu_island_3.png"
                    }else{
                        buliding = "material/island/bu_island_3.png"
                    }
                    if (isiland_name === 'Food') {
                        buliding = "material/island/food/bu_island_3.png"
                    }
                    if (isiland_name === 'School') {
                        buliding = "material/island/school/bu_island_3.png"
                    }
                } else if (isiland_recod_percentage < 0 ) {
                    if (isiland_name === 'Animals and Insects') {
                        buliding = "material/island/animals/bu_island_2.png"
                    }else{
                        buliding = "material/island/bu_island_2.png"
                    }
                    if (isiland_name === 'Food') {
                        buliding = "material/island/food/bu_island_2.png"
                    }
                    if (isiland_name === 'School') {
                        buliding = "material/island/school/bu_island_2.png"
                    }
                } else {
                    if (isiland_name === 'Animals and Insects') {
                        buliding = "material/island/animals/bu_island_1.png"
                    }else{
                        buliding = "material/island/bu_island_1.png"
                    }
                    if (isiland_name === 'Food') {
                        buliding = "material/island/food/bu_island_1.png"
                    }
                    if (isiland_name === 'School') {
                        buliding = "material/island/school/bu_island_1.png"
                    }
                }


                let new_context = " <div id='island-" + (island_number+1) + "' class='island' name='"+isiland_name+"'> "
                +"<img class='island-image' src='"+buliding+"'><p>" +
                "  <div id='island-name-" + (island_number+1) + "' class='island-name'>" +isiland_name
                +" </div></div>";
                // 塞到html裡面
                $("#slide_"+(index+1)).append(new_context);
                island_number++;
            }
        }
        $('body').loading('stop');
    }
    
    /*---------------------------------------------------------------------*/
    
    
    
    
    await getPersonalInformation();
    await show_this_page_island();
    /*---------------------------------------------------------------------*/
    var myFullpage = new fullpage('#stages', {
        resize: true,
        slidesNavigation: true,
        slidesNavPosition: "bottom",
        lazyLoading: true,
        css3: true,
        paddingTop: '0px',
        paddingBottom: '20px'
    });
    $(".fullpage-wrapper").css("height", "auto");
    $(".section fp-auto-height").css("height", "350px")
    /*---------------------------------------------------------------------*/
    
    /*進入島嶼*/
    $('.island').on('click', function () {
        let name = $('#'+this.id).attr('name');
        location.replace("topic_challenge_pick.html?name=" + name);
    });

    /*進入背包*/
    $('#bag').on('click', function () {
        console.log('將要進入 背包 ');
        window.location.assign("bag.html");
    });

     /*進入錯誤單字*/
     $('#book').on('click', function () {
        console.log('將要進入 背包 ');
        window.location.assign("wrong_book.html");
    });

    /*進入勳章*/
    $('#rank').on('click', function () {
        console.log('將要進入 背包 ');
        window.location.assign("rank.html");
    });

    /*上一頁*/
    $('#boat').on('click', function () {
        console.log('將回到登入畫面。');
        dialog(0);
    });


    /*點頭向動物*/
    $('#shops').on('click', function () {
        console.log('將要進入 更衣間 ');
        window.location.assign("locker_room.html");
    });
    
});