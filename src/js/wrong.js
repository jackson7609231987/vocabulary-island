$(function () {
    /* 跳出通知。*/
    function dialog(status) {
        let icon = "";
        let title = "";
        let text = "";
        let console_log = "";
        if (status == 0) {
            icon = "warning";
            title = "將前往";
            text = "是否前往【登入畫面】？";
            swal.fire({
                    icon: icon,
                    title: title,
                    text: text,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes !'
                })
                .then((result) => {
                    if (result.value) {
                        window.location.assign("index.html");
                    }
                });

        } else if (status == 1) {
            icon = "warning";
            title = "哎呀！發現你沒登入哦！";
            text = "將前往【登入畫面】";

            swal.fire({
                    icon: icon,
                    title: title,
                    text: text,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCloseButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'O K'
                })
                .then((result) => {
                    if (result.value) {
                        window.location.assign("index.html");
                    }
                });
        }






    }
    /*抓使用者資訊。*/
    function getPersonalInformation() {
        console.log("開始抓使用者資訊...");

        $.ajax({
            type: "get",
            async: true, //async設定true會變成異步請求。
            cache: true,
            url: "php/world.php",
            dataType: "json",
            success: function (json) {
                //jQuery會自動將結果傳入(如果有設定callback函式的話，兩者都會執行)
                console.log('Success.');
                name = json['name'];
                number_of_card = json['cardAmount'];
                coin = json['coin'];

                console.log('Name:' + name);

                if (name == "" || name == null) { // 帳號未登入。
                    dialog(1);
                }

                $('#name').text(name);
                $('#flashcard').text(number_of_card);
                $('#coin').text(coin);

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
                console.log("textStatus:" + textStatus);
                console.log("errorThrown:" + errorThrown);
                console.log('Error.');

                console.log('Name:' + name);

                dialog(1);

                number_of_card = 0;
                $('#name').text(name);
                $('#flashcard').text(number_of_card);
            }
        });
        /* 未完成 */
    }
    let words = []; // 存放單字的陣列。
    /* 抓取字卡所需資訊。*/
    function get_card() {

        $.ajax({
            type: "POST",
            async: true, //async設定true會變成異步請求。
            cache: true,
            url: "php/wrong_word.php",
            data: {
                code: 0
            },
            dataType: "json",
            success: function (json) {
                //jQuery會自動將結果傳入(如果有設定callback函式的話，兩者都會執行)
                console.log(json);
                console.log(json.length + " 筆資料");

                let context = "";

                for (let i = 0; i < json.length; i++) {
                    /* 字卡資訊的所需變數。 */
                    let word = json[i]['wt_vocabulary'];
                    let part_speech = json[i]['vl_part_of_speech'];
                    let definition = json[i]['vl_definition'];

                    /* 抓語音所需變數 */
                    words[i] = word;

                    let new_context = "<div  class='card' style='border: 8px solid '><p class='vocabulary'>" + word 
                    + "</p>"+"<span class='part_speech'>" + part_speech + "</span><span class='definition'>" + definition 
                    +"</span><br><input type='image' class='normal_sound' data-target='" +
                    word +"' src='material/speaker.png'>"+ 
                    "<img class='picture' src='word_image/" 
                    + word + "_0.png' alt=''></div>";

                    context = context + new_context;


                }

                /* 渲染於瀏覽器上。 */
                $('#card_pile').append(context);
                $('body').loading('stop');

                /* 將陣列中重複的單字剃除。 */
                let result = {};
                let repeat = {};
                words.forEach(function (item) {
                    result.hasOwnProperty(item) ? repeat[item]++ : result[item]++;
                });
                words = Object.keys(result);
                console.log(words);

                /* 抓取標準語音。 */
                for (let i = 0; i < words.length; i++) {
                    get_normal_audio(words[i]);
                }

            },
            error: function (error) {
                console.log(error.responseText);
                alert('get_card : Wrong。');

            }
        });
    }
    /* 抓取字卡所需 標準 語音。*/
    function get_normal_audio(word) {
        // 先載入資料夾，直接使用語音檔。
        let sound;
        let regex = /\s/;
        let vocabulary = word;
        vocabulary = vocabulary.replace(regex, '');
        console.log('vocabulary:' + vocabulary);
        sound = document.createElement("audio"); //創建聲音檔元件。
        sound.setAttribute("id", "sound_" + vocabulary);
        sound.setAttribute("src", "word_sound/" + vocabulary + ".mp3");
        sound.setAttribute("preload", "auto");
        document.body.appendChild(sound);
    }

    /* 回去英文單字島。 */
    $('#earth').on('click', function () {
        console.log('將進入 - 英文單字島');
        window.location.assign("world.html");
    });
    /* 點擊標準語音 */
    $(document).on('click', '.normal_sound', function () {
        console.log('PLAY...');
        let target = $(this).data('target');
        let regex = /\s/;
        target = target.replace(regex, '');
        target = '#sound_' + target;
        console.log('target:' + target);
        var playPromise = $(target).get(0).play();
        if (playPromise !== undefined) {
            playPromise.then(_ => {
                    $(target).get(0).pause();
                })
                .catch(error => {
                    console.log(error);
                    // Auto-play was prevented
                    // Show paused UI.
                });
        }
        var timeout_0 = setTimeout(function () {
            $(target).get(0).play(); /*播放一次語音*/
        }, 300);

    });
    getPersonalInformation();

    get_card();
    /*轉頁效果。*/
    var myFullpage = new fullpage('#stages', {
        resize: true,
        slidesNavigation: true,
        slidesNavPosition: "bottom",
        lazyLoading: true,
        css3: true,
        paddingTop: '30px',
        paddingBottom: '50px'
    });


});