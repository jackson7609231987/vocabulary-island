<?php
    session_start();
    include('connMysql.php');
    
    $information = array();         //最後回傳的資訊陣列。

    $title = $_GET['title'];        //標題。
    $account = $_SESSION['user'];   //使用者帳號。


    /* 從標題抓取關卡資訊(代碼) */
    $sql_find_level_code = "SELECT * FROM vocabularyisland.title WHERE title_name = :title_name";
    $stmt = $pdo->prepare($sql_find_level_code);
    $stmt->bindValue(':title_name',$title); // 避免SQL injection。
    $stmt->execute() or exit("讀取 title 資料表時，發生錯誤。"); //執行。 
    $row = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
    $title_id = $row[0]['title_id'];
    $theme_id = $row[0]['theme_id'];
    $information['title_id'] = $title_id;
    $information['theme_id'] = $theme_id;


    /* 抓取自主練習的總數量 */
    // 查找練習表
    $sql_find_practice_amount = "SELECT Count(*) AS num FROM vocabularyisland.practice WHERE title_id = :title_id ";
    $stmt = $pdo->prepare($sql_find_practice_amount);
    $stmt->bindValue(':title_id',$title_id); // 避免SQL injection。
    $stmt->execute() or exit("讀取 practice 資料表時，發生錯誤。"); //執行。 
    $row = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
    $information['amount_practices'] = $row[0]['num']; // 總自主練習數量。

    /* 抓取每個自主練習的標題 ( 依照關卡代碼順序由小至大 ) */
    $title_practices = array();
    for( $i=0 ; $i < $information['amount_practices'] ; $i++ ){
        $sql_find_practice_title = "SELECT pt_id,pt_name FROM vocabularyisland.practice WHERE title_id = :title_id  ORDER BY title_id ASC";
        $stmt = $pdo->prepare($sql_find_practice_title);
        $stmt->bindValue(':title_id',$title_id);     // 避免SQL injection。
        $stmt->execute() or exit("讀取 practice 資料表時，發生錯誤。"); //執行。 
        $row = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
        $title_practices[$i]['pt_name'] = $row[$i]['pt_name'];
        $title_practices[$i]['pt_id'] = $row[$i]['pt_id'];

        // 確認數據庫有這個等級的單字才顯示出來關卡
        $sql = "SELECT COUNT(*) AS num FROM `vocabulary_library` WHERE `practice_id` = :practice_id";
        $_stmt = $pdo->prepare($sql);
        $_stmt->bindValue(':practice_id',$row[$i]['pt_id']); 
        $_stmt->execute() or exit("讀取 practice 資料表時，發生錯誤。"); //執行。 
        $_row = $_stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
        
        // 確認關卡有單字
        if ($_row[0]['num'] < 1 ) {
            // 沒單字把她刪除

            unset($title_practices[$i]);
        }
    }
    $information['title_practices'] = $title_practices; // 每個自主練習的名稱。
    

    

    /* 從標題抓取闖關紀錄資訊(代碼) */
    $practice_records = array();
    for( $i=0 ; $i< count($information['title_practices']) ; $i++ ){
        $sql = "SELECT * FROM vocabularyisland.practice_status WHERE ps_account = :ps_account AND theme_id = :theme_id AND title_id = :title_id AND practice_id = :practice_id ORDER BY ps_save_date DESC";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':ps_account',$account); // 避免SQL injection。
        $stmt->bindValue(':theme_id',$theme_id); // 避免SQL injection。
        $stmt->bindValue(':title_id',$title_id); // 避免SQL injection。
        $stmt->bindValue(':practice_id',$information['title_practices'][$i]['pt_id']); // 避免SQL injection。
        $stmt->execute() or exit("讀取 practice_status 資料表時，發生錯誤。"); //執行。 
        $row = $stmt->fetchALL(PDO::FETCH_ASSOC);
        $Rows = Count($row);
        for( $j=0 ; $j<$Rows ; $j++ ){
            // 遊玩的時間
            $information['title_practices'][$i]['practice_records'][$j] =  $row[$j]['ps_save_date'];
        }

         // 查詢學過的單字量
         $sql_find_process = "SELECT 1 AS record_totle_count FROM `record` WHERE au_account = :ACCOUNT AND  title_id = :title_id AND practice_id = :practice_id GROUP BY au_vocabulary";
         $stmt = $pdo->prepare($sql_find_process);
         $stmt->bindValue(':ACCOUNT',$account); 
         $stmt->bindValue(':title_id',$title_id);
         $stmt->bindValue(':practice_id',$information['title_practices'][$i]['pt_id']); // 避免SQL injection。
         $stmt->execute() or exit("讀取practice_status，發生問題。");
         $row = $stmt->fetchALL(PDO::FETCH_ASSOC); // 全部取出
         $Rows = Count($row);
         if ($Rows < 1) {
            $information['title_practices'][$i]['record_totle_count'] = 0;
         }else{
            $information['title_practices'][$i]['record_totle_count'] =$Rows;
         }

         // 查詢關卡總單字量
         $sql_find_process = "SELECT COUNT(1) AS totle_count FROM `vocabulary_library`  WHERE title_id = :title_id AND practice_id = :practice_id";
         $stmt = $pdo->prepare($sql_find_process);
         $stmt->bindValue(':title_id',$title_id);
         $stmt->bindValue(':practice_id',$information['title_practices'][$i]['pt_id']); // 避免SQL injection。
         $stmt->execute() or exit("讀取practice_status，發生問題。");
         $row = $stmt->fetchALL(PDO::FETCH_ASSOC); // 全部取出
         $Rows = Count($row);
         $information['title_practices'][$i]['totle_count'] = $row[0]['totle_count'];
        
    }

    

// 查詢學過的單字
        $sql_find_process = "SELECT 1 AS record_totle_count FROM `record` WHERE au_account = :ACCOUNT AND  title_id = :title_id GROUP BY au_vocabulary";
        $stmt = $pdo->prepare($sql_find_process);
        $stmt->bindValue(':ACCOUNT',$account); 
        $stmt->bindValue(':title_id',$title);
        $stmt->execute() or exit("讀取practice_status，發生問題。");
        $row = $stmt->fetchALL(PDO::FETCH_ASSOC); // 全部取出
        $Rows = Count($row);
        if ($Rows < 1) {
            $information['island'][$i]['record_totle_count'] = 0;
        }else{
            $information['island'][$i]['record_totle_count'] =$Rows;
        }

    
    // /* 抓取使用者每個自主練習的星數 ( 依照關卡代碼順序由小至大 ) */
    // $star_practices = array();
    // /* 抓取使用者每個自主練習的代碼 ( 依照關卡代碼順序由小至大 ) */
    // $code_practices = array();
    // for( $i=0 ; $i< count($information['title_practices']) ; $i++ ){
    //     $sql_find_practice_time = "SELECT * FROM vocabularyisland.practice_status WHERE ps_account = :ps_account AND theme_id = :theme_id AND title_id = :title_id AND practice_id = :practice_id ORDER BY title_id ASC";
    //     $stmt = $pdo->prepare($sql_find_practice_time);
    //     $stmt->bindValue(':ps_account',$account); // 避免SQL injection。
    //     $stmt->bindValue(':theme_id',$theme_id); // 避免SQL injection。
    //     $stmt->bindValue(':title_id',$title_id); // 避免SQL injection。
    //     $stmt->bindValue(':practice_id',$information['title_practices'][$i]['pt_id']); // 避免SQL injection。
        
    //     $stmt->execute() or exit("讀取 practice_status 資料表時，發生錯誤。"); //執行。 
    //     $row = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
    //     $star_practices[$i] = count($row); // 看遊玩幾次，代表幾顆星。
    //     $code_practices[$i] = $theme_id."-".$title_id."-".$information['title_practices'][$i]['pt_id'];
    //     $information['title_practices'][$i]['star_count'] =count($row);
    //     $information['title_practices'][$i]['star_practices'] =$theme_id."-".$title_id."-".$information['title_practices'][$i]['pt_id'];
    // }

    // $information['star_practices'] = $star_practices; // 每個自主練習進度(星數)。
    // $information['code_practices'] = $code_practices; // 每個自主練習進度(代碼)。




    $pdo = null;

    echo json_encode($information);

    /* 闖關進度 */
    // 等單字庫及關卡確定後再說
?>