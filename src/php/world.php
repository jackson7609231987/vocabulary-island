<?php
    session_start();
    include('connMysql.php');
    
    $information = array();
    $account = $_SESSION["user"];
    $user_id = $_SESSION['user_id'];
    

    /* 使用者姓名 */
    $sql = "SELECT user_name, user_coin,user_wardrobe,user_character,wardrobe_id,wardrobe_name,wardrobe_img_url FROM vocabularyisland.member LEFT JOIN member_wardrobe ON wardrobe_id = user_wardrobe WHERE user_account = :ACCOUNT";
    $stmt = $pdo->prepare($sql);
    $stmt->bindValue(':ACCOUNT',$account); // 避免SQL injection。
    $stmt->execute() or exit("讀取member資料表時，發生錯誤。"); //執行。 
    $row = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
    $information['name'] = $row[0]['user_name'];
    $information['coin'] = $row[0]['user_coin'];
    $information['user_wardrobe_id'] = $row[0]['user_wardrobe'];
    $information['wardrobe_name'] = $row[0]['wardrobe_name'];
    $information['user_character'] = $row[0]['user_character'];
    $information['wardrobe_img_url'] = $row[0]['wardrobe_img_url'];


    $sql = "SELECT * FROM `member_wardrobe` WHERE `member_id` = :member_id";
    $stmt = $pdo->prepare($sql);
    $stmt->bindValue(':member_id',$user_id); // 避免SQL injection。
    $stmt->execute() or exit("讀取member資料表時，發生錯誤。"); //執行。 
    $row = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
    $information['member_wardrobe'] =  $row;


    /* 所有島嶼分類 */
    $sql = "SELECT * FROM `theme`";
    $stmt = $pdo->prepare($sql);
    $stmt->execute() or exit("讀取member資料表時，發生錯誤。"); //執行。 
    $row = $stmt->fetchALL(PDO::FETCH_ASSOC);
    $information['theme']= $row;

    /* 所有島嶼 前三關要先完成*/
    $sql = "SELECT * FROM `title` WHERE `title_id` IN (3,7,9);";
    $stmt = $pdo->prepare($sql);
    $stmt->execute() or exit("讀取member資料表時，發生錯誤。"); //執行。 
    $row = $stmt->fetchALL(PDO::FETCH_ASSOC);
    $information['island']= $row;


    $sql = "SELECT * FROM `title` WHERE `title_id` not IN (3,7,9);";
    $stmt = $pdo->prepare($sql);
    $stmt->execute() or exit("讀取member資料表時，發生錯誤。"); //執行。 
    $row = $stmt->fetchALL(PDO::FETCH_ASSOC);
    $information['island']= array_merge($information['island'], $row);;




    /* 字卡數量 */
    $sql_find_personal_information = "SELECT count(*) as num FROM vocabularyisland.card WHERE author = :ACCOUNT";
    $stmt = $pdo->prepare($sql_find_personal_information);
    $stmt->bindValue(':ACCOUNT',$account); // 避免SQL injection。
    $stmt->execute() or exit("讀取card資料表時，發生錯誤。"); //執行。 
    $row = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
    $information['cardAmount'] = $row[0]['num'];

    /* 子主題進度(顯示建築物狀態) */
    for ($i=0; $i < count($information['island']) ; $i++) { 
        $title = $information['island'][$i]['title_id'];
        $sql_find_process = "SELECT COUNT(1) AS totle_count FROM `vocabulary_library`  WHERE title_id = :title_id";
        $stmt = $pdo->prepare($sql_find_process);
        $stmt->bindValue(':title_id',$title);
        $stmt->execute() or exit("讀取practice_status，發生問題。");
        $row = $stmt->fetchALL(PDO::FETCH_ASSOC); // 全部取出
        $Rows = Count($row);
        $information['island'][$i]['totle_count'] = $row[0]['totle_count'];


        // 查詢學過的單字
        $sql_find_process = "SELECT 1 AS record_totle_count FROM `record` WHERE au_account = :ACCOUNT AND  title_id = :title_id GROUP BY au_vocabulary";
        $stmt = $pdo->prepare($sql_find_process);
        $stmt->bindValue(':ACCOUNT',$account); 
        $stmt->bindValue(':title_id',$title);
        $stmt->execute() or exit("讀取practice_status，發生問題。");
        $row = $stmt->fetchALL(PDO::FETCH_ASSOC); // 全部取出
        $Rows = Count($row);
        if ($Rows < 1) {
            $information['island'][$i]['record_totle_count'] = 0;
        }else{
            $information['island'][$i]['record_totle_count'] =$Rows;
        }


    }



    $pdo = null;
    echo json_encode($information);

    /* 闖關進度 */
    // 等單字庫及關卡確定後再說。

?>