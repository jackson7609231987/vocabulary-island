<?php
    session_start();
    $code = $_POST['code']; 
    $user_id = $_SESSION['user_id'];
    $all_shop_item = [
        [
            'name' => 'hat_1',
            'img_url' => './material/locker-room/hat/資產 1.png',
            'coin'  => '5',
        ],
        [
            'name' => 'hat_2',
            'img_url' => './material/locker-room/hat/資產 2.png',
            'coin'  => '10',
        ],
        [
            'name' => 'hat_3',
            'img_url' => './material/locker-room/hat/資產 3.png',
            'coin'  => '15',
        ],
        [
            'name' => 'hat_4',
            'img_url' => './material/locker-room/hat/資產 4.png',
            'coin'  => '20',
        ],
        [
            'name' => 'hat_5',
            'img_url' => './material/locker-room/hat/資產 5.png',
            'coin'  => '25',
        ],
        [
            'name' => 'hat_6',
            'img_url' => './material/locker-room/hat/資產 6.png',
            'coin'  => '30',
        ],
        [
            'name' => 'hat_7',
            'img_url' => './material/locker-room/hat/資產 7.png',
            'coin'  => '35',
        ],
        [
            'name' => 'hat_9',
            'img_url' => './material/locker-room/hat/資產 9.png',
            'coin'  => '40',
        ],
        [
            'name' => 'hat_10',
            'img_url' => './material/locker-room/hat/資產 10.png',
            'coin'  => '45',
        ],
        [
            'name' => 'hat_11',
            'img_url' => './material/locker-room/hat/資產 11.png',
            'coin'  => '50',
        ],
        [
            'name' => 'hat_12',
            'img_url' => './material/locker-room/hat/資產 12.png',
            'coin'  => '55',
        ],
      
      
    ];
    include('connMysql.php');
    if ($code == 'GET_ALL') {
        echo json_encode($all_shop_item);
    } else if($code == 'BUY_HAT'){
        //查找商品物件
        $wardrobe_name = $_POST['wardrobe_name'];
        $iteam = findObjectById($all_shop_item,$wardrobe_name);
        if ($iteam) {
            // 確認餘額
            // 扣錢
            // 新增一筆到櫥窗
            $pdo->beginTransaction();
            // 新增除窗
            $sql_1 ="INSERT INTO `member_wardrobe`(`member_id`, `wardrobe_img_url`, `wardrobe_name`) VALUES ('".$user_id."','".$iteam['img_url']."','".$iteam['name']."')";
            $stmt = $pdo->prepare($sql_1);
            try {
                if($stmt->execute()){
                    $last_id = $pdo->lastInsertId();
                    error_log( $last_id);
                    error_log( $user_id);
                    error_log( $iteam['coin']);
                    // 新增用戶穿哪件衣服
                    $sql_2 ="UPDATE member SET user_coin=user_coin-".$iteam['coin'].",user_wardrobe=".$last_id." WHERE user_id=".$user_id." AND user_coin+".$iteam['coin'].">=0";

                    error_log( $sql_2);
                    $stmt_2 = $pdo->prepare($sql_2);
                    $count = $stmt_2->execute();
                    error_log($count);
                    if($count !='0'){
                        $pdo->commit();
                        echo json_encode(['msg'=>'done']);
                    }else{

                    }
                }
            } catch (Throwable $e) {
                // An exception has been thrown
                // We must rollback the transaction
                $pdo->rollback();
            }
        }else{
            // 沒查到商品
            echo '沒查到商品';
        }
    } else if ($code == 'CHANHE_HAT'){
        $wardrobe_name = $_POST['wardrobe_name'];

        $sql ="UPDATE member SET user_wardrobe=(SELECT `wardrobe_id` FROM `member_wardrobe` WHERE `member_id`=:ACCOUNT AND `wardrobe_name` = :wardrobe_name) WHERE user_id=:ACCOUNT";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':ACCOUNT',$user_id); // 避免SQL injection。
        $stmt->bindValue(':wardrobe_name',$wardrobe_name); // 避免SQL injection。
        $stmt->execute();
        echo json_encode(['msg'=>'done']);
        
    } else if ($code == 'CHANHE_CHARACTER') {
        $character_code = $_POST['character_code'];
        if(strpos(' 01234',strval($character_code)) == false){
            echo json_encode(['msg'=>'character_code is not find']);
            return;
        }
        $sql ="UPDATE member SET user_character=:character_code WHERE user_id=:ACCOUNT";
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':ACCOUNT',$user_id); // 避免SQL injection。
        $stmt->bindValue(':character_code',$character_code); // 避免SQL injection。
        $stmt->execute();
        echo json_encode(['msg'=>'done']);
    }

    function findObjectById($array, $name){
        foreach ( $array as $element ) {
            if ( $name == $element['name'] ) {
                return $element;
            }
        }
        return false;
    }
        
?>