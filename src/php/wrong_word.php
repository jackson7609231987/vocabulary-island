<?php
    session_start();
    include('connMysql.php');

    
    $information = array();                   // 最後回傳的資訊陣列。
    $account = $_SESSION["user"];             // 使用者帳號。


        $sql = "SET SESSION sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY,',''));";
        $stmt = $pdo->prepare($sql);
        $stmt->execute();


        $sql = "SELECT * FROM `exp_wrong_time` ".
        "INNER JOIN vocabulary_library ON exp_wrong_time.wt_vocabulary = vocabulary_library.vl_vocabulary ".
        "WHERE exp_wrong_time.`wt_account`=:wt_account GROUP BY `wt_vocabulary` ORDER BY `wt_save_date` DESC;";

        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(':wt_account',$account);

         /* 回傳狀態。*/
        if ($stmt->execute()) {
            $information = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將資料照索引順序一一全部取出，並以陣列放入。

        } else {
            $information = $stmt->error;
        }
        
        $pdo = null;
        echo json_encode($information);
        

?>