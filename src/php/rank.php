<?php
    session_start();
    include('connMysql.php');
    
    $information = array();                   // 最後回傳的資訊陣列。
    $account = $_SESSION["user"];             // 使用者帳號。

    $sql = "
    SELECT `user_name`,user_character,(select wardrobe_name from member_wardrobe where member_wardrobe.wardrobe_id = member.user_wardrobe) AS user_wardrobe,card_id  FROM `member` JOIN card ON card.author = user_account
    ";

    $stmt = $pdo->prepare($sql);

    function compareObjects( $a , $b )
    {
        return strcmp(count($b),count($a));
    }
    /* 回傳狀態。*/
    if ($stmt->execute()) {
        $information = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將資料照索引順序一一全部取出，並以陣列放入。
        $data = array();
        foreach ($information as $item) {
            $key = $item['user_name']; // or $item['info_id']
            if (!isset($data[$key])) {
                $data[$key] = array();
            }
            $data[$key]['user_name'] = $item['user_name'];
            $data[$key]['user_wardrobe'] = $item['user_wardrobe'];
            $data[$key]['user_character'] = $item['user_character'];
            $data[$key]['cards'][]['card_id'] = $item['card_id'];
        }
        foreach ($data as $key =>  $item) {
            $data[$key]['count'] = count($item['cards']);
        }
        $row = array();
        foreach ($data as $item) {
            $row[]= $item;
        }
        //这行代码是取所有排名前10位
        // $data = array_slice($data,0,10);
        $information = $row;
        
    } else {
        $information = $stmt->error;
    }
    
    $pdo = null;
    echo json_encode($information);

?>