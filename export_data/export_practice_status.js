var mysql      = require('mysql');
const { query } = require('./async-db')
let account ='s3030501'
let all_user_data = [];

async function get_data(account) {
    let user_data=[];
    // 獲取紀錄 
    let all_ps = await get_practice_status(account);
    for (let index = 0; index < all_ps.length; index++) {
        const element = all_ps[index];
        let ps_save_date = element.ps_save_date;
        let ps_data = {
            event:'PRACTICE',
            date:ps_save_date,
            context:[],
            // by_step_context:{
            //     learn:{},
            //     spell_word_1:{},
            //     spell_word_2:{},
            //     draw:{

            //     }
            // }
        }
        let SR_DATA = await get_exp_select_record(ps_save_date,account);
        let CS_DATA = await get_exp_click_sound(ps_save_date,account);
        let DR_DATA = await get_exp_delete_record(ps_save_date,account);
        let CT_DATA = await get_exp_click_tip(ps_save_date,account);
        let WT_DATA = await get_exp_wrong_time(ps_save_date,account);
        /**
            單字聽念  0 4 1  2
         ********************************************** 
         */
        for (let word_count = 0; word_count < 4; word_count++) {
            // console.log(CS_DATA[3]);
            // 聽單字
            ps_data.context[0+word_count*4]={
                type:'PLAY_SOUND',
                tims:CS_DATA[2+word_count*3].cs_click_time,
            };
            // 錄音
            ps_data.context[1+word_count*4]={
                tpye:'RECORDING',
            };
            // 選擇錄音
            ps_data.context[2+word_count*4]={
                tpye:'SELETC_RECORDING_TARGET',
                target:SR_DATA[word_count].sr_select_target
            };
            // 刪除錄音次數
            ps_data.context[3+word_count*4]={
                tpye:'DELETE_RECORDING_TIMES',
                tims:DR_DATA[word_count].dr_delete_time
            };
            /**
             第一次拼單字   0 3 5 
             *********************************************** 
             */
            ps_data.context[4+word_count*4]={
                type:'PLAY_SOUND',
                tims:CS_DATA[1+word_count*3].cs_click_time+1,
            };
            ps_data.context[5+word_count*4]={
                tpye:'CLICK_TIP',
                tims:CT_DATA[1+word_count*2].ct_click_time,
            };
            ps_data.context[6+word_count*4]={
                type:'WRONG_TIMES',
                tims:WT_DATA[1+word_count*2].wt_wrong_time,
            };
            /**
             第2次拼單字   0 3 5 
             *********************************************** 
             */
             ps_data.context[7+word_count*4]={
                type:'PLAY_SOUND',
                tims:CS_DATA[word_count*3].cs_click_time+1,
            };
            ps_data.context[8+word_count*4]={
                tpye:'CLICK_TIP',
                tims:CT_DATA[0+word_count*2].ct_click_time,
            };
            ps_data.context[9+word_count*4]={
                type:'WRONG_TIMES',
                tims:WT_DATA[0+word_count*2].wt_wrong_time,
            };
            // 以上重複四次
        }
        /**
         * 畫畫
         */
        ps_data.context[22]={
            type:'DRAWING',
        };
        /**
         * 訂製資料
         */



        await user_data.push(ps_data);
    }
    // 0 = exp_click_sound
    // 1 = exp_select_record
    // 2 = exp_delete_record
    // 3 = exp_click_tip
    // 4 = record
    // 5 = exp_wrong_time
    // 7 = practice_status
    
    return user_data;
    
    // 做字卡

}

get_data(account).then(result => {
            all_user_data.push({account:result});
            console.log(
                JSON.stringify(all_user_data)
            );
        }
    );
















async function get_practice_status(account) {
    let results = await query(`SELECT * FROM vocabularyisland.practice_status where ps_account = '${account}' order by ps_id asc`);
    return results;
}

async function get_exp_select_record(ps_save_date,account) {
    let results = await query(`SELECT * FROM vocabularyisland.exp_select_record where sr_account = '${account}' and sr_save_date <= '${ps_save_date}'  order by sr_id desc limit 4 `);
    return results;
}


async function get_exp_click_sound(ps_save_date,account) {
    let results = await query(`SELECT * FROM vocabularyisland.exp_click_sound where cs_account = '${account}' and cs_save_date <= '${ps_save_date}'  order by cs_id desc limit 12 `);
    return results;
}

async function get_exp_delete_record(ps_save_date,account) {
    let results = await query(`SELECT * FROM vocabularyisland.exp_delete_record where dr_account = '${account}' and dr_save_date <= '${ps_save_date}'  order by dr_id desc limit 4 `);
    return results;
}


async function get_exp_click_tip(ps_save_date,account) {
    let results = await query(`SELECT * FROM vocabularyisland.exp_click_tip where ct_account = '${account}' and ct_save_date <= '${ps_save_date}'  order by ct_id desc limit 8`);
    return results;
}

async function get_exp_wrong_time(ps_save_date,account) {
    let results = await query(`SELECT * FROM vocabularyisland.exp_wrong_time where wt_account = '${account}' and wt_save_date <= '${ps_save_date}'  order by wt_id desc limit 8`);
    return results;
}