FROM php:7.2-apache
COPY ./src/ /var/www/html/
COPY php.ini /usr/local/etc/php/php.ini-production
RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
RUN chmod 777 /var/www/html/upload/image
RUN chmod 777 /var/www/html/upload/sound
EXPOSE 80