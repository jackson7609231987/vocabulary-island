var fs = require('fs'),
    fetch = require('node-fetch');
const readXlsxFile = require('read-excel-file/node');
const cheerio = require("cheerio");
const puppeteer = require('puppeteer');

// 輸出到檔案
var download = async function (uri, filename) {
    // await request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    console.log(uri);
    const res = await fetch(uri);
    if (checkStatus(res)) {
        await new Promise((resolve, reject) => {
            const fileStream = fs.createWriteStream('./download_sound/' + filename+'.mp3');
            res.body.pipe(fileStream);
            res.body.on("error", (err) => {
                process.exit(1);
            });
            fileStream.on("finish", function () {
                resolve();
            });
        });
    }


};

function checkStatus(res) {
    if (res.ok) { // res.status >= 200 && res.status < 300
        return res;
    } else {
        console.error(res.statusText);
        process.exit(1);
    }
}

// download('https://s.cdict.net/c/bar.mp3');


//  excel 的設置
const schema = {
    'vocabulary': {
        prop: 'vocabulary',
        type: String
    },
    'definition': {
        prop: 'definition',
        type: String

    },
    'speech': {
        prop: 'speech',
        type: String
    },
    'title': {
        prop: 'title',
        type: String,
    },
    'level': {
        prop: 'level',
        type: String,
    }
}

readXlsxFile('./分8級單字表_1.xlsx', {
    // 填入分頁
    sheet: 11 ,
    // 設定
    schema
}).then(async ({
    rows,
    errors
}) => {
    // 確認沒有錯誤
    errors.length === 0
    console.log(rows);
    for (let index = 0; index < rows.length; index++) {
        const row = rows[index];
        var name = row.vocabulary.toLowerCase();
        var fist_letter = name.charAt(0);
        // https://tw.dictionary.search.yahoo.com/search?p=america
        // var url = 'https://s.cdict.net/' + fist_letter + '/' + name + '.mp3';
        // await download(url, name)
        await download_puppeteer(name);
    }
});


var download_puppeteer = async function (name) {
    let url = "https://tw.dictionary.search.yahoo.com/search?p="+name;
    const browser = await puppeteer.launch({
        headless: false, // 不使用 headless 模式，就會開啟瀏覽器來實際動作
        slowMo: 150, // 每個動作的間隔時間，方便觀察實際動作
      });
    const page = await browser.newPage();
    await page.goto(url);
    const bodyHandle = await page.$('body');
    const html = await page.evaluate((body) => body.innerHTML, bodyHandle);
    await bodyHandle.dispose();
// console.log(html);
    const $ = cheerio.load(html);
    const table_tr = $('audio'); // 爬最外層的 Table(class=BoxTable) 中的 tr
    await browser.close();
    await download(table_tr.attr('src'), name);
}


// download_puppeteer('zoom').then(()=>{
//     // console.log('...');
//   });

