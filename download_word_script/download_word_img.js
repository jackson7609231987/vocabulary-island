var Scraper = require('images-scraper');
var fs = require('fs'),
    fetch = require('node-fetch');
const google = new Scraper({
    puppeteer: {
        headless: false,
    },
});
const readXlsxFile = require('read-excel-file/node');


// 把圖片輸出到檔案
var download = async function (uri, filename) {
    // await request(uri).pipe(fs.createWriteStream(filename)).on('close', callback);
    console.log(uri);
    const res = await fetch(uri);
    await new Promise((resolve, reject) => {
        const fileStream = fs.createWriteStream('./download_img/' + filename);
        res.body.pipe(fileStream);
        res.body.on("error", (err) => {
            process.exit(1);
        });
        fileStream.on("finish", function () {
            resolve();
        });
    });

};


// 設置;
google.options = {
    userAgent: 'Mozilla/5.0 (X11; Linux i686; rv:64.0) Gecko/20100101 Firefox/64.0', // the user agent
    puppeteer: {}, // puppeteer options, for example, { headless: false }
    tbs: { // every possible tbs search option, some examples and more info: http://jwebnet.net/advancedgooglesearch.html
        itp: "photo" // options: clipart, face, lineart, news, photo
    },
    safe: false // enable/disable safe search
};


// 從 google 抓圖
var scrapeImg = async function (name) {
    const results = await google.scrape(name+' png' , 3 );
    console.log('img_namee: '+name+'  img_url', results[0].url);
    await download(results[0].url, name + '_0.png').catch(e => {
        console.log(e);
    });
    await download(results[1].url, name + '_1.png').catch(e => {
        console.log(e);
    });
    await download(results[2].url, name + '_2.png').catch(e => {
        console.log(e);
    });
    console.log(name+"完成");
};

//  excel 的設置
const schema = {
    'vocabulary': {
        prop: 'vocabulary',
        type: String
    },
    'definition': {
        prop: 'definition',
        type: String

    },
    'speech': {
        prop: 'speech',
        type: String
    },
    'title': {
        prop: 'title',
        type: String,
    },
    'level': {
        prop: 'level',
        type: String,
    }
}

readXlsxFile('./分8級單字表_1.xlsx', {
    sheet: 11,
    schema
}).then(async ({
    rows,
    errors
}) => {
    errors.length === 0
    console.log(rows);
   for (let index = 0; index < rows.length; index++) {
       const row = rows[index];
       await scrapeImg(row.vocabulary)
   }
});




//$env:PUPPETEER_EXECUTABLE_PATH='C:\Program Files\Mozilla Firefox\firefox.exe'
//$env:PUPPETEER_EXECUTABLE_PATH="C:\Program Files (x86)\Google\Chrome\Application\chrome.exe"