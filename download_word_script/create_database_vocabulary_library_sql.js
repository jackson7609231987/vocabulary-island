var fs = require('fs'),
    fetch = require('node-fetch');
const readXlsxFile = require('read-excel-file/node');


// download('https://s.cdict.net/c/bar.mp3');

// 01 人 People
// 02 量測 Measure
// 03 食物 Food
// 04 興趣 Hobby
// 05 物品 Thing
// 06 運輸 Transport
// 07 學校 School
// 08 場所 Place
// 09 動物與昆蟲 Animals and Insects
// 10 假日與自然 Holiday and Nature
// 11 名詞 Noun
// 12 動詞 Verb
// 13 形容詞 Adjective
// 14 副詞與其他 Adverb and Other
const title_schema = {
    'People': {
        title_id: 1,
    },
    'Measure': {
        title_id: 2,
    },
    'Food': {
        title_id: 3,
    },
    'Hobby': {
        title_id: 4,
    },
    'Thing': {
        title_id: 5,
    },
    'Transport': {
        title_id: 6,
    },
    'School': {
        title_id: 7,
    },
    'Place': {
        title_id: 8,
    },
    'Animals and Insects': {
        title_id: 9,
    },
    'Holiday and Nature': {
        title_id: 10,
    },
    'Noun': {
        title_id: 11,
    },
    'Verb': {
        title_id: 12,
    },
    'Adjective': {
        title_id: 13,
    },
    'Adverb and Other': {
        title_id: 14,
    },
}
const keys = Object.keys(title_schema);

//  excel 的設置
const schema = {
    'vocabulary': {
        prop: 'vocabulary',
        type: String
    },
    'definition': {
        prop: 'definition',
        type: String

    },
    'speech': {
        prop: 'speech',
        type: String
    },
    'title': {
        prop: 'title',
        type: String,
    },
    'level': {
        prop: 'level',
        type: String,
    }
}

// for (let index = 0; index < 1; index++) {
//     keys.forEach((key, keysIndex) => {
//         console.log("INSERT INTO `title`(`title_id`, `title_name`, `theme_id`) VALUES ('"+title_schema[key].title_id+"','"+key+"','"+(index+1)+"');");
//     });
    
// }
// let id = 1 
// for (let index = 0; index < 8; index++) {
//     let sql_value = ""
//     keys.forEach(async(key, keysIndex) => {
//          sql_value += "('"+id+"','L - "+(index+1)+"','"+title_schema[key].title_id+"'),";
//         await id++;
//     });
//     sql_value=sql_value.substring(0,sql_value.length-1)
//     console.log("INSERT INTO `practice`(`pt_id`, `pt_name`, `title_id`) VALUES "+sql_value+";");
// }
let sheet_num = 11;
readXlsxFile('./分8級單字表_1.xlsx', {
    sheet: sheet_num ,
    schema
}).then(async ({
    rows,
    errors
}) => {
    errors.length === 0
    console.log(rows);
    for (let index = 0; index < rows.length; index++) {
        var row = rows[index];
        var vocabulary = row.vocabulary;
        var definition = row.definition;
        var speech = row.speech;
        var title_id = title_schema[row.title].title_id;
        var SQL = "INSERT INTO `vocabulary_library`( `vl_vocabulary`, `vl_definition`, `vl_part_of_speech`, `title_id`, `practice_id`, `vl_upload_date`) VALUES " +
            "('"+vocabulary+"','"+definition+"','"+speech+"','"+title_id+"',(SELECT `pt_id` FROM `practice` WHERE pt_name = 'L - "+row.level+"' AND`title_id` = "+title_id+"),NOW());"
        console.log(SQL);

    }
});