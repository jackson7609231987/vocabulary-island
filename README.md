// 製作image
sudo docker build -t my-php-app .

// 不啟動掛載
sudo docker run --name my-php-apache -d -p 8080:80 my-php-app

// 把掛載檔案放到本地
sudo docker run --name my-php-apache -d -p 8080:80 -v  ~/upload:/var/www/html/upload my-php-app


// 強制刪除
sudo docker rm -f  my-php-apache
// 刪除IMAGE
sudo docker image rm my-php-app

sudo docker exec -it my-php-apache bash